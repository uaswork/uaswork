# UASwork

UASwork is a repository of software to support UAS operations.

The software is copyrighted by the copyright owners listed in the file license.txt in the respective software directories. 

To promote open use most of the software has been released as permissive free open source software. Please see license.txt in the respective software directories for more information.

The repository is maintained by the SDU UAS Center at the University of Southern Denmark https://sdu.dk/uas

For information please contact Kjeld Jensen <kj@uaswork.org>

